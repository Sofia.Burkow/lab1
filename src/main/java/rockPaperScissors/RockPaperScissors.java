package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    Random r = new Random();
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");

    
    public void run() {
        // TODO: Implement Rock Paper Scissors

        System.out.println("Let's play round " + roundCounter);

        while (true) {

            String userChoice = readInput("Your choice (Rock/Paper/Scissors)?").toLowerCase();
            if (!rpsChoices.contains(userChoice)) {
                System.out.println("I do not understand " + userChoice + ". Could you try again?");
                continue;
            }

            String computerChoice = randomChoice(rpsChoices);

            System.out.print("Human chose " + userChoice + ", computer chose " + computerChoice + ". ");

            if (userChoice.equals(computerChoice)) {
                System.out.println("It's a tie!");
            } else if (computerChoice == "rock") {
                if (userChoice.equals("paper")) {
                    System.out.println("Human wins!");
                    humanScore++;
                } else {
                    System.out.println("Computer wins!");
                    computerScore++;
                }
            } else if (computerChoice == "paper") {
                if (userChoice.equals("scissors")) {
                    System.out.println("Human wins!");
                    humanScore++;
                } else {
                    System.out.println("Computer wins!");
                    computerScore++;
                }

            } else { // if scissors
                if (userChoice.equals("rock")) {
                    System.out.println("Human wins!");
                    humanScore++;
                } else {
                    System.out.println("Computer wins!");
                    computerScore++;
                }
            }

            System.out.println("Score: human " + humanScore + ", computer " + computerScore);

            String anotherOne = readInput("Do you wish to continue playing? (y/n)?");
            if (!anotherOne.equals("y")) {
                System.out.println("Bye bye :)");
                break;
            }
            roundCounter++;
            System.out.println("Let's play round " + roundCounter);
        }
    }

    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }

    /**
     * Random choice between rock, paper and scissors
     */
    public String randomChoice(List<String> rpsChoices) {
        int randomItem = r.nextInt(rpsChoices.size());
        return rpsChoices.get(randomItem);
    }

}
